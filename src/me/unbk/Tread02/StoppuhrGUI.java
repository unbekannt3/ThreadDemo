package me.unbk.Tread02;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class StoppuhrGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2108197370847531152L;
	private JPanel contentPane;
	public static boolean timer1Active = false;	
	public Threads t1 = new Threads("timer1");
	public static JLabel timer1_label;
	public static JTextPane txtpnStopBeforeReset;
	public static long milis = 0;
	public static long seconds = 0;
	public static long minutes = 0;
	public static long hours = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StoppuhrGUI frame = new StoppuhrGUI();
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StoppuhrGUI() {

		// Natives System Design
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}	

		setTitle("Thread02 - Stoppuhr");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 150);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panel_timer1 = new JPanel();
		tabbedPane.addTab("Timer", null, panel_timer1, null);
		panel_timer1.setLayout(null);

		// Reset Button initialisieren
		JButton timer1_buttonReset = new JButton("Reset");

		timer1_label = new JLabel("0");
		timer1_label.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		timer1_label.setHorizontalAlignment(SwingConstants.CENTER);
		timer1_label.setBounds(12, 13, 395, 34);
		panel_timer1.add(timer1_label);

		JButton timer1_buttonStart = new JButton("Start");
		timer1_buttonStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				timer1Active = true;
				t1.start();
				timer1_buttonReset.setEnabled(false);
			}
		});
		timer1_buttonStart.setBounds(20, 60, 90, 23);
		panel_timer1.add(timer1_buttonStart);

		JButton timer1_buttonStop = new JButton("Stop");
		timer1_buttonStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				timer1Active = false;
				timer1_buttonReset.setEnabled(true);

			}
		});
		timer1_buttonStop.setBounds(165, 60, 90, 23);
		panel_timer1.add(timer1_buttonStop);

		// Reset Button Funktionen
		timer1_buttonReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!timer1Active) {
					t1.reset();
					minutes = 0;
					hours = 0;
					updateGui(0);
				}
			}
		});

		timer1_buttonReset.setBounds(310, 60, 90, 23);
		panel_timer1.add(timer1_buttonReset);

		JTextPane txtpnStopBeforeReset = new JTextPane();
		txtpnStopBeforeReset.setEditable(false);
		txtpnStopBeforeReset.setEnabled(false);
		txtpnStopBeforeReset.setText("Stop before Reset");
		txtpnStopBeforeReset.setBounds(165, 0, 94, 20);
		panel_timer1.add(txtpnStopBeforeReset);

	}

	public static void updateGui(int timer) {
		milis = timer;
		
		hours = TimeUnit.MILLISECONDS
		    .toHours(milis);

		minutes = TimeUnit.MILLISECONDS
		    .toMinutes(milis);

		seconds = TimeUnit.MILLISECONDS
		    .toSeconds(milis);

		String time = "" + String.format("%02d", hours) + ":" + String.format("%02d", minutes % 60) + ":" + String.format("%02d", seconds % 60) + ":" + String.format("%03d", milis % 1000);
		timer1_label.setText(time);
	}
}
