package me.unbk.Tread02;

public class Threads implements Runnable {

	private String threadName;
	private int zeit = 0;
	private Thread thread;

	Threads(String name) {
		super();
		threadName = name;
		System.out.println("[constructor] Erstelle Thread " +  threadName );

	}

	@Override
	public void run() {
		System.out.println("[run] " + threadName + " gestartet");
		try {
			while(StoppuhrGUI.timer1Active) {
				//System.out.println("[run] Thread: " + threadName + ", " + zeit);
				Thread.sleep(1);
				zeit += 1;
				StoppuhrGUI.updateGui(zeit);
			}
		} catch (InterruptedException e) {
			System.out.println("[run] Thread " +  threadName + " unterbrochen: " + e);
		}
		System.out.println("[run] Thread " +  threadName + " beendet.");

	}
	public void start() {
		this.thread = new Thread(this);
		this.thread.start();
	}

	public void stop() {
		this.thread.interrupt();
		this.thread = null;
	}

	public void reset() {
		this.zeit = 0;
	}

}
