package me.unbk.Tread01;

public class ThreadDemo implements Runnable {
    private Thread t;
    private String threadName;
    protected 

    ThreadDemo( String name) {
        threadName = name;
        System.out.println("[constructor] Erstelle Thread " +  threadName );
    }

    public void run() {
        System.out.println("[run] " + threadName + " gestartet");
        try {
            // Von 4 runterz�hlen in 0,5 sek. schritten
            for(int i = 4; i > 0; i--) {
                System.out.println("[run] Thread: " + threadName + ", " + i);
                // Thread f�r 500ms pausieren.
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("[run] Thread " +  threadName + " unterbrochen: " + e);
        }
        System.out.println("[run] Thread " +  threadName + " beendet.");
    }

    public void start () {
        System.out.println("[start] Starte " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
